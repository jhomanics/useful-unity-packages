﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class EventTriggerHandler : MonoBehaviour {

    public static void AddEventTrigger(GameObject obj, EventTriggerType triggerType, UnityAction<BaseEventData> action)
    {
        EventTrigger trigger = obj.GetComponent<EventTrigger>();
        if (!trigger)
            trigger = obj.AddComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = triggerType;
        entry.callback = new EventTrigger.TriggerEvent();
        UnityAction<BaseEventData> call = new UnityAction<BaseEventData>(action);
        entry.callback.AddListener(call);
        trigger.triggers.Add(entry);
    }
   
}
